class ChangeActivationTokenOnUser < ActiveRecord::Migration[5.2]
  def change
    rename_column :users, :activation_token, :activation_token_digest
  end
end
