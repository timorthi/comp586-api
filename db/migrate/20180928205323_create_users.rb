class CreateUsers < ActiveRecord::Migration[5.2]
  def change
    create_table :users do |t|
      t.string :first_name
      t.string :last_name
      t.string :preferred_name
      t.date :date_of_birth
      t.string :activation_token
      t.datetime :activated_at
      t.datetime :activation_sent_at
      t.string :email
      t.string :password_digest

      t.timestamps
    end
  end
end
