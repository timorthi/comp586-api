puts '1/3 Seeding users...'
2.times do |i|
  first_name = Faker::Name.first_name
  User.create!(
    first_name: first_name,
    last_name: Faker::Name.last_name,
    preferred_name: first_name,
    email: "seed_user#{i}@gmail.com",
    password_digest: BCrypt::Password.create('password'),
    activation_token_digest: Digest::SHA256.new.hexdigest('TOKEN'),
    activation_sent_at: Time.zone.now,
    activated_at: (i % 2 == 0) ? Time.zone.now : nil
  )
end

puts '2/3 Seeding todo lists for each user...'
User.all.each do |user|
  3.times do
    user.todo_lists.build(
      title: "My #{Faker::Color.color_name.capitalize} List",
      description: Faker::Lorem.sentence
    )
  end
  user.save!
end

puts '3/3 Seeding todo items for each todo list...'
TodoList.all.each do |todo_list|
  2.times do
    todo_list.todo_items.build(
      description: "Feed #{Faker::Dog.name}",
      status: :done
    )

    todo_list.todo_items.build(
      description: "Buy #{Faker::Food.dish}",
      status: :pending
    )
  end
  todo_list.save!
end
