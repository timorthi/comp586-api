require 'rails_helper'

RSpec.describe 'AuthenticationService' do
  let(:user) { create(:user) }
  let(:valid_user_jwt) { JsonWebToken.encode(user_id: user.id) }
  let(:random_jwt) { 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiYWRtaW4iOnRydWUsImp0aSI6IjdhYzRhMzcyLTM2OGItNGQyMi1hNzdlLTU1NzUyOWRmMGUzYyIsImlhdCI6MTUzOTkwODYzNywiZXhwIjoxNTM5OTEyMjM3fQ._w1PWnxTCjkZOHgjqHZZyjEPwgSY2wVBWKwJSECk10U' }

  describe '#call' do
    let(:service) { AuthenticationService.new }

    context 'with valid jwt and payload' do
      it 'returns a user' do
        allow(service).to receive(:jwt_from_http_header).and_return(valid_user_jwt)
        expect(service.call).to eq user
      end
    end

    context 'with valid jwt but invalid payload' do
      it 'returns nil' do
        allow(service).to receive(:payload_from_jwt).and_return('bad': 'payload')
        expect(service.call).to be_nil
      end
    end

    context 'with no jwt' do
      it 'returns nil' do
        allow(service).to receive(:jwt_from_http_header)
        expect(service.call).to be_nil
      end
    end
  end

  describe '#jwt_from_http_header' do
    context 'with valid auth headers' do
      let(:service) { AuthenticationService.new("Bearer #{valid_user_jwt}") }
      it 'extracts the jwt' do
        expect(service.send(:jwt_from_http_header)).to eq valid_user_jwt
      end
    end

    context 'with invalid headers' do
      let(:service) { AuthenticationService.new }
      it 'returns nil' do
        expect(service.send(:jwt_from_http_header)).to be_nil
      end
    end
  end

  describe '#payload_from_jwt' do
    let(:service) { AuthenticationService.new }

    context 'with valid jwt originating from this app' do
      it 'returns decoded payload' do
        result = service.send(:payload_from_jwt, valid_user_jwt)
        expect(result[:user_id]).to eq user.id
      end
    end

    context 'with valid jwt not originating from this app' do
      it 'returns an empty hash' do
        expect(service.send(:payload_from_jwt, random_jwt)).to eq({})
      end
    end

    context 'with jwt undefined' do
      it 'returns an empty hash' do
        expect(service.send(:payload_from_jwt, nil)).to eq({})
      end
    end
  end
end
