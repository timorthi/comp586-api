RSpec.describe 'Token' do
  let(:valid_length) { 10 }
  let(:invalid_length) { 0 }

  describe '#generate' do
    context 'with invalid params' do
      context 'when length is not large enough' do
        it 'raises ArgumentError' do
          expect { Token.generate(invalid_length) }.to raise_error ArgumentError
        end
      end
    end

    context 'with valid params' do
      let(:result) { Token.generate(valid_length) }
      it 'returns a dictionary raw value and hashed value of the generated string' do
        expect(result).to have_key :raw
        expect(result).to have_key :digest
      end

      it 'returns raw token of specified length' do
        expect(result[:raw].length).to eq valid_length
      end

      it 'returns raw token with all uppercase' do
        expect(result[:raw]).to eq result[:raw].upcase
      end

      it 'returns hashed value of the raw token' do
        expect(Token::HASH_FUNC.hexdigest(result[:raw])).to eq result[:digest]
      end
    end
  end

  describe '#verify' do
    let(:result) { Token.generate(valid_length) }

    context 'when correct raw token is provided' do
      it 'returns true' do
        expect(Token.verify(result[:raw], result[:digest])).to eq true
        expect(Token.verify(result[:raw].downcase, result[:digest])).to eq true
      end
    end

    context 'when incorrect raw token is provided' do
      it 'returns false' do
        expect(Token.verify('someBadToken', result[:digest])).to eq false
      end
    end
  end

end
