RSpec.describe 'Token' do
  let(:payload) { { user_id: 1, email: 'john@smith.com' } }

  describe '#encode' do
    it 'encodes a payload' do
      expect(JsonWebToken.encode(payload)).to be_truthy
    end
    it 'rejects empty payloads' do
      expect { JsonWebToken.encode }.to raise_error ArgumentError
    end
  end

  describe '#decode' do
    let(:jwt) { JsonWebToken.encode(payload) }
    context 'with valid token' do
      let(:result) { JsonWebToken.decode(jwt) }
      it 'decodes the token' do
        expect(result).to be_truthy
      end
    end
    it 'returns nil for invalid tokens' do
      expect(JsonWebToken.decode('bad_format')).to be_nil
    end
  end
end
