FactoryBot.define do
  factory :todo_list do
    user
    title { "TestTodoList" }
    description { "MyString" }
  end
end
