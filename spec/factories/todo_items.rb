FactoryBot.define do
  factory :todo_item do
    description { "Test Todo Item" }
    status { 1 }
    todo_list
  end
end
