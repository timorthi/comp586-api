FactoryBot.define do
  factory :user do
    first_name { 'Tester' }
    last_name { 'Smith' }
    preferred_name { 'SuperTester' }
    sequence(:email) { |n| "tester_#{n}@smith.com" }
    password { 'password' }

    trait :activated do
      activated_at { '2018-09-28 00:00:00' }
    end

    trait :with_todo_lists do
      transient do
        todo_lists_count { 5 }
      end

      after(:create) do |user, evaluator|
        create_list(:todo_list, evaluator.todo_lists_count, user: user)
      end
    end
  end
end
