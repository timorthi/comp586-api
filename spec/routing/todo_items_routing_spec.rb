require 'rails_helper'

RSpec.describe TodoItemsController, type: :routing do
  describe 'routing' do
    it 'routes to #create' do
      expect(post: '/todo_lists/1/todo_items').to route_to('todo_items#create', todo_list_id: '1')
    end

    it 'routes to #update' do
      expect(put: '/todo_items/1').to route_to('todo_items#update', id: '1')
    end

    it 'routes to #destroy' do
      expect(delete: '/todo_items/1').to route_to('todo_items#destroy', id: '1')
    end
  end
end
