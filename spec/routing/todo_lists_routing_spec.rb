require 'rails_helper'

RSpec.describe TodoListsController, type: :routing do
  describe 'routing' do
    it 'routes to #index' do
      expect(get: '/users/1/todo_lists').to route_to('todo_lists#index', user_id: '1')
    end

    it 'routes to #create' do
      expect(post: '/users/1/todo_lists').to route_to('todo_lists#create', user_id: '1')
    end

    it 'routes to #show' do
      expect(get: '/todo_lists/2').to route_to('todo_lists#show', id: '2')
    end
  end
end
