require 'rails_helper'

RSpec.describe User, type: :model do
  describe 'callbacks' do
    let(:user) { build(:user) }
    it 'calls #downcase_email before save' do
      expect(user).to receive(:downcase_email)
      user.save
    end
  end

  describe '#activated?' do
    let(:user) { build(:user, :activated) }
    let(:unactivated_user) { build(:user) }

    context 'activated_at is present' do
      it 'returns true' do
        expect(user.activated?).to eq true
      end
    end

    context 'activated_at is not present' do
      it 'returns false' do
        expect(unactivated_user.activated?).to eq false
      end
    end
  end

  describe '#activate!' do
    let(:token) { Token.generate(User::ACTIVATION_TOKEN_LENGTH) }
    let(:activated_user) { build(:user, :activated) }
    let(:new_user) { build(:user, activation_token_digest: token[:digest]) }

    context 'already activated' do
      it 'adds to errors base' do
        activated_user.activate!(token[:raw])
        expect(activated_user.errors[:base]).to eq ['User is already activated']
      end
      it 'does not change the activated_at field' do
        expect { new_user.activate!('some_bad_token') }.not_to change(new_user, :activated_at)
      end
    end

    context 'with invalid token' do
      it 'adds to errors base' do
        new_user.activate!('some_bad_token')
        expect(new_user.errors[:base]).to eq ['Incorrect activation token']
      end
      it 'does not change the activated_at field' do
        expect { new_user.activate!('some_bad_token') }.not_to change(new_user, :activated_at)
      end
    end

    context 'with valid token' do
      it 'updates the activated_at field with current timestamp' do
        new_user.activate!(token[:raw])
        expect(new_user.activated_at).to be_within(1.second).of Time.zone.now
      end
    end
  end

  describe '#downcase_email' do
    let(:uppercase_email) { 'Test@USER.com' }
    let(:user) { build(:user, email: uppercase_email) }
    it 'forces email to all lowercase' do
      user.send(:downcase_email)
      expect(user.email).to eq uppercase_email.downcase!
    end
  end
end
