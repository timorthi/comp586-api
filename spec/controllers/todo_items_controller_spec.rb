require 'rails_helper'
require 'json'

RSpec.describe TodoItemsController, type: :controller do
  render_views
  let(:todo_list) { create(:todo_list) }
  let(:todo_item) { create(:todo_item, status: :pending, todo_list_id: todo_list.id) }
  let(:other_todo_item) { create(:todo_item) }
  let(:other_todo_list) { other_todo_item.todo_list }

  before :each do
    allow(controller).to receive(:authenticate_request).and_return true
    allow(controller).to receive(:current_user).and_return todo_list.user
  end

  describe 'POST #create' do
    let(:valid_attributes) { { description: 'Test Task' } }
    let(:invalid_attributes) { { description: '' } }

    context 'when todo_list belongs to another user' do
      it 'returns forbidden' do
        post :create, params: { todo_list_id: other_todo_list.id, todo_item: valid_attributes }
        expect(response).to have_http_status :forbidden
      end
    end

    context 'with valid params' do
      it 'returns success' do
        post :create, params: { todo_list_id: todo_list.id, todo_item: valid_attributes }
        expect(response).to have_http_status :created
      end
      it 'saves a todo_item for the todo_list' do
        expect { post :create, params: { todo_list_id: todo_list.id, todo_item: valid_attributes } }
          .to change { todo_list.todo_items.count }
          .by 1
      end
      it 'returns the created todo_list in the response' do
        post :create, params: { todo_list_id: todo_list.id, todo_item: valid_attributes }
        res = JSON.parse(response.body)
        expect(res['todo_item']).to be_truthy
      end
    end

    context 'with invalid params' do
      it 'returns error' do
        post :create, params: { todo_list_id: todo_list.id, todo_item: invalid_attributes }
        expect(response).to have_http_status :unprocessable_entity
      end
    end
  end

  describe 'PUT/PATCH #update' do
    let(:valid_attributes) { { status: 'done' } }
    let(:invalid_attributes) { { status: 'not_an_enum' } }

    context 'when todo_list belongs to another user' do
      it 'returns forbidden' do
        put :update, params: { id: other_todo_item.id, todo_item: valid_attributes }
        expect(response).to have_http_status :forbidden
      end
    end

    context 'with valid params' do
      before :each do
        put :update, params: { id: todo_item.id, todo_item: valid_attributes }
      end
      it 'returns success' do
        expect(response).to have_http_status :ok
      end
      it 'updates the status' do
        expect(todo_item.reload.status).to eq valid_attributes[:status]
      end
      it 'returns the created todo_list in the response' do
        res = JSON.parse(response.body)
        expect(res['todo_item']).to be_truthy
      end
    end

    context 'with invalid params' do
      it 'returns error' do
        put :update, params: { id: todo_item.id, todo_item: invalid_attributes }
        expect(response).to have_http_status :bad_request
      end
    end
  end

  describe 'DELETE #destroy' do
    context 'when todo_list belongs to another user' do
      it 'returns forbidden' do
        delete :destroy, params: { id: other_todo_item.id }
        expect(response).to have_http_status :forbidden
      end
    end

    context 'with valid id' do
      before :each do
        delete :destroy, params: { id: todo_item.id }
      end
      it 'returns success' do
        expect(response).to have_http_status :ok
      end
      it 'deletes the todo item' do
        expect(TodoItem.find_by_id(todo_item.id)).to be_nil
      end
    end
  end
end
