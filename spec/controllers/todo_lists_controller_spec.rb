require 'rails_helper'
require 'json'

RSpec.describe TodoListsController, type: :controller do
  render_views
  let(:user) { create(:user, :with_todo_lists, :activated) }

  before :each do
    allow(controller).to receive(:authenticate_request).and_return true
    allow(controller).to receive(:current_user).and_return user
  end

  describe 'GET #index' do
    context "with other user's id" do
      it 'returns forbidden' do
        get :index, params: { user_id: user.id+1 }
        expect(response).to have_http_status :forbidden
      end
    end

    context 'with correct user id' do
      before :each do
        get :index, params: { user_id: user.id }
      end
      it 'returns success' do
        expect(response).to have_http_status :ok
      end
      it 'returns the users todos in the response body' do
        res = JSON.parse(response.body)
        expect(res['todo_lists']).to be_truthy
        expect(res['todo_lists'].length).to eq user.todo_lists.count
      end
      it 'returns the todos count in the response body' do
        res = JSON.parse(response.body)
        expect(res['count']).to eq user.todo_lists.count
      end
      it 'returns the user id in the response body' do
        res = JSON.parse(response.body)
        expect(res['user_id']).to eq user.id
      end
    end
  end

  describe 'POST #create' do
    let(:valid_attributes) { attributes_for(:todo_list) }
    let(:invalid_attributes) { { description: '' } }

    context "with other user's id" do
      it 'returns forbidden' do
        post :create, params: { user_id: user.id+1, todo_list: valid_attributes }
        expect(response).to have_http_status :forbidden
      end
    end

    context 'with valid params' do
      it 'returns success' do
        post :create, params: { user_id: user.id, todo_list: valid_attributes }
        expect(response).to have_http_status :created
      end
      it 'saves a todo_list for the user' do
        expect { post :create, params: { user_id: user.id, todo_list: valid_attributes } }
          .to change { user.todo_lists.count }
          .by 1
      end
      it 'returns the created todo_list in the response' do
        post :create, params: { user_id: user.id, todo_list: valid_attributes }
        res = JSON.parse(response.body)
        expect(res['todo_list']).to be_truthy
      end
    end

    context 'with invalid params' do
      before :each do
        post :create, params: { user_id: user.id, todo_list: invalid_attributes }
      end
      it 'returns error' do
        expect(response).to have_http_status :unprocessable_entity
      end
    end
  end

  describe 'GET #show' do
    context 'when todo_list belongs to another user' do
      it 'returns forbidden' do
        todo_list_id = create(:todo_list).id
        get :show, params: { id: todo_list_id }
        expect(response).to have_http_status :forbidden
      end
    end

    context 'with correct todo_list id' do
      let(:todo_list_id) { user.todo_lists.first.id }
      before :each do
        get :show, params: { id: todo_list_id }
      end
      it 'returns success' do
        expect(response).to have_http_status :ok
      end
      it 'returns the todo_list' do
        res = JSON.parse(response.body)
        expect(res['todo_list']['id']).to eq todo_list_id
      end
    end

    context 'with incorrect todo_list id' do
      before :each do
        get :show, params: { id: -1 }
      end
      it 'returns not found' do
        expect(response).to have_http_status :not_found
      end
    end
  end
end
