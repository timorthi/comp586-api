require 'rails_helper'
require 'json'

RSpec.describe UsersController, type: :controller do
  let(:valid_attributes) { attributes_for(:user) }
  let(:invalid_attributes) { { bad: 'attributes' } }

  before :each do
    allow(controller).to receive(:authenticate_request).and_return true
  end

  describe 'GET #show' do
    context 'with valid user id' do
      it 'returns a success response' do
        user = create(:user)
        get :show, params: { id: user.id }
        expect(response).to have_http_status :ok
      end
    end
    context 'with invalid user id' do
      it 'returns 404' do
        get :show, params: { id: -1 }
        expect(response).to have_http_status :not_found
      end
    end
  end

  describe 'POST #create' do
    context 'with valid params' do
      render_views

      it 'creates a new User' do
        expect { post :create, params: { user: valid_attributes } }.to change(User, :count).by(1)
      end

      it 'renders a JSON response with the new user' do
        post :create, params: { user: valid_attributes }
        expect(response).to have_http_status :created
        expect(response.content_type).to eq 'application/json'
      end

      it 'sets activation details for the user' do
        post :create, params: { user: valid_attributes }
        user = User.last
        expect(user.activation_token_digest).to be_truthy
        expect(user.activation_sent_at).to be_within(1.second).of Time.zone.now
      end

      it 'returns a JSON response with the jwt' do
        post :create, params: { user: valid_attributes }
        response_hash = JSON.parse(response.body)
        expect(response_hash['jwt']).to be_truthy
      end
    end

    context 'with invalid params' do
      it 'renders a JSON response with errors for the new user' do
        post :create, params: { user: invalid_attributes }
        expect(response).to have_http_status :unprocessable_entity
        expect(response.content_type).to eq 'application/json'
      end
    end
  end

  describe 'PUT/PATCH #update' do
    let(:token) { Token.generate(User::ACTIVATION_TOKEN_LENGTH) }
    let(:user) { create(:user, activation_token_digest: token[:digest]) }

    before :each do
      allow(controller).to receive(:current_user).and_return user
    end

    context 'when user id does not match current user' do
      it 'returns 403 forbidden' do
        allow(controller).to receive(:current_user).and_return build(:user) # Stub current_user.id to nil
        put :update, params: { id: user.id, user: { activation_token: token[:raw] } }
        expect(response).to have_http_status :forbidden
      end
    end

    context 'with valid activation token' do
      it 'activates the user' do
        put :update, params: { id: user.id, user: { activation_token: token[:raw] } }
        expect(response).to have_http_status :ok
        expect(user.activated_at).to be_within(1.second).of Time.zone.now
      end
    end

    context 'with invalid activation token' do
      it 'returns error' do
        put :update, params: { id: user.id, user: { activation_token: 'BadToken' } }
        expect(response).to have_http_status :unprocessable_entity
        expect(user.activated_at).to eq nil
      end
    end

    context 'with invalid params format' do
      it 'returns error' do
        put :update, params: { id: user.id, user: { something: '' } }
        expect(response).to have_http_status :bad_request
      end
    end
  end
end
