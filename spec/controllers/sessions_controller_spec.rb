require 'rails_helper'
require 'json'

RSpec.describe SessionsController, type: :controller do
  let(:user) { create(:user) }

  describe '#create' do
    before :each do
      post :create, params: { user: login_params }
    end

    context 'with correct login information' do
      let(:login_params) { { email: user.email, password: 'password' } }
      render_views

      it 'succeeds' do
        expect(response).to have_http_status :created
      end
      it 'returns a json object with jwt and user keys' do
        response_hash = JSON.parse(response.body)
        expect(response_hash['jwt']).to be_truthy
        expect(response_hash['user']).to be_truthy
      end
    end

    context 'with incorrect email' do
      let(:login_params) { { email: 'wrong_email', password: 'password' } }
      it 'returns unauthorized' do
        expect(response).to have_http_status :unauthorized
      end
    end

    context 'with incorrect password' do
      let(:login_params) { { email: user.email, password: 'wrong_password' } }
      it 'returns unauthorized' do
        expect(response).to have_http_status :unauthorized
      end
    end
  end
end
