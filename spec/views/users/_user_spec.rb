require 'rails_helper'

RSpec.describe 'users/_user', type: :view do
  let(:user) { create(:user) }

  it 'contains essential user information' do
    assign(:user, user)
    render

    expect(rendered).to match /id/
    expect(rendered).to match /first_name/
    expect(rendered).to match /last_name/
    expect(rendered).to match /preferred_name/
    expect(rendered).to match /email/
    expect(rendered).to match /activated_at/
  end
end
