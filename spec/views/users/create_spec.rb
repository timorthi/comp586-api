require 'rails_helper'

RSpec.describe 'users/create', type: :view do
  let(:user) { create(:user) }
  let(:jwt) { JsonWebToken.encode(user_id: user.id) }

  it 'contains essential user information' do
    assign(:user, user)
    assign(:jwt, jwt)
    render

    expect(rendered).to match /user/
    expect(rendered).to match /jwt/
  end
end
