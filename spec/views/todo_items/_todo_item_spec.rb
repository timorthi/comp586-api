require 'rails_helper'

RSpec.describe 'todo_items/_todo_item', type: :view do
  let(:todo_item) { create(:todo_item) }

    it 'contains essential todo_item information' do
      assign(:todo_item, todo_item)
      render

      expect(rendered).to match /id/
      expect(rendered).to match /description/
      expect(rendered).to match /status/
    end
end
