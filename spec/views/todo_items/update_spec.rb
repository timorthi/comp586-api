require 'rails_helper'

RSpec.describe 'todo_items/update', type: :view do
  let(:todo_item) { create(:todo_item) }

  it 'contains essential todo_item information after update' do
    assign(:todo_item, todo_item)
    render
    expect(rendered).to match /todo_item/
  end
end
