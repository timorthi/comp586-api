require 'rails_helper'

RSpec.describe 'todo_items/create', type: :view do
  let(:todo_list) { create(:todo_list) }
  let(:todo_item) { create(:todo_item, todo_list_id: todo_list.id) }

  it 'contains essential todo_item information after create' do
    assign(:todo_list, todo_list)
    assign(:todo_item, todo_item)
    render

    expect(rendered).to match /todo_list/
    expect(rendered).to match /todo_item/
  end
end
