require 'rails_helper'

RSpec.describe 'todo_lists/index', type: :view do
  let(:user) { create(:user, :with_todo_lists) }

  it 'contains a list of todos and metadata' do
    assign(:user, user)
    assign(:todo_lists, user.todo_lists)
    render

    expect(rendered).to match /user_id/
    expect(rendered).to match /count/
    expect(rendered).to match /todo_lists/
  end
end
