require 'rails_helper'

RSpec.describe 'todo_lists/show', type: :view do
  let(:user) { create(:user, :with_todo_lists) }

  context 'with user' do
    it 'contains a user_id' do
      assign(:user, user)
      assign(:todo_list, user.todo_lists.first)
      render
      expect(rendered).to match /user_id/
    end
  end

  context 'without user' do
    it 'contains a todo_list' do
      assign(:todo_list, user.todo_lists.first)
      render
      expect(rendered).to match /todo_list/
    end
  end
end
