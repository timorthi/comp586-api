require 'rails_helper'

RSpec.describe 'todo_lists/_todo_list', type: :view do
  let(:todo_list) { create(:todo_list) }

  it 'contains essential user information' do
    assign(:todo_list, todo_list)
    render

    expect(rendered).to match /id/
    expect(rendered).to match /title/
    expect(rendered).to match /description/
  end
end
