Rails.application.routes.draw do
  resources :users, except: %i[index destroy]
  resources :sessions, only: :create

  resources :users do
    resources :todo_lists, only: %i[index create]
  end

  resources :todo_lists, only: %i[show] do
    resources :todo_items, only: :create
  end

  resources :todo_items, only: %i[update destroy]
end
