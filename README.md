# Timothy Ng's COMP586 API
This is a Ruby on Rails application. It serves as the back-end API for the course project in Prof. Rabinovich's COMP586 class for Fall 2018.

---

## Installation
Using Docker and Docker Compose, you can get the project up and running via
```bash
$ docker-compose up
```

Otherwise, install the following:
* Ruby 2.5.1
* Rails 5.2
* PostgreSQL 9

Then run the setup script and start the development server:
```bash
$ bin/setup && bin/start
```

The API can then be accessed with root `localhost:9000`.

---

## Source Code
The main source code can be found in `app/`.

---

## Tests
Tests can be found in `spec/`. Run tests using
```bash
$ bundle exec rspec
```
