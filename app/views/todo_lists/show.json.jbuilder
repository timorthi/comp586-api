json.user_id @user.id if @user.present?

json.todo_list do
  json.partial! 'todo_lists/todo_list'
  json.todo_items @todo_list.todo_items.each do |todo_item|
    @todo_item = todo_item
    json.partial! 'todo_items/todo_item'
  end
end
