json.user_id @user.id
json.count @todo_lists.count

json.todo_lists @todo_lists.each do |todo_list|
  @todo_list = todo_list
  json.partial! 'todo_lists/todo_list'
end
