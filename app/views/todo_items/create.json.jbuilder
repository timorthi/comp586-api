json.todo_list_id @todo_list.id

json.todo_item do
  json.partial! 'todo_items/todo_item'
end
