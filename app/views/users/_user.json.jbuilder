json.extract! @user,
              :id,
              :first_name,
              :last_name,
              :preferred_name,
              :email,
              :activated_at
