json.user do
  json.partial! 'users/user'
end

json.jwt @jwt
