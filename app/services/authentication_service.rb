# Helper service class that accepts an Authorization header,
# retrieves the bearer token (assumed to be JWT), and tries to decode it.
class AuthenticationService
  def initialize(auth_header = nil)
    @auth_header = auth_header
  end

  # Driver method for the service.
  #
  # @return user - a User model corresponding to the user ID in the JWT, or nil
  def call
    jwt = jwt_from_http_header
    payload = payload_from_jwt(jwt)
    User.find_by_id(payload.dig(:user_id))
  end

  private

  # Tries to split the header string and returns the last token
  def jwt_from_http_header
    @auth_header&.split(' ')&.last
  end

  # Tries to decode the given JWT and returns the payload or an empty hash if
  # decoding failed
  def payload_from_jwt(jwt)
    JsonWebToken.decode(jwt) || {}
  end
end
