# == Schema Information
#
# Table name: users
#
#  id                      :bigint(8)        not null, primary key
#  activated_at            :datetime
#  activation_sent_at      :datetime
#  activation_token_digest :string
#  email                   :string
#  first_name              :string
#  last_name               :string
#  password_digest         :string
#  preferred_name          :string
#  created_at              :datetime         not null
#  updated_at              :datetime         not null
#
# Indexes
#
#  index_users_on_email  (email)
#

class User < ApplicationRecord
  has_secure_password

  ACTIVATION_TOKEN_LENGTH = 5

  validates :email, uniqueness: true
  validates :first_name, :last_name, :email, presence: true
  validates :first_name, :last_name, format: { with: /\A[^0-9`!@#\$%\^&*+_=]+\z/ }
  validates :preferred_name, format: { with: /\A[^0-9`!@#\$%\^&*+_=]+\z/ }, if: -> { preferred_name.present? }
  validates :password, length: { minimum: 6 }, if: -> { password.present? }

  before_save :downcase_email

  has_many :todo_lists

  def activated?
    activated_at.present?
  end

  def activate!(raw_token)
    if activated?
      errors.add(:base, 'User is already activated')
    elsif not Token.verify(raw_token, activation_token_digest)
      errors.add(:base, 'Incorrect activation token')
    end
    return false if errors.any?
    update_attributes(activated_at: Time.zone.now)
  end

  private

  def downcase_email
    email.downcase!
  end
end
