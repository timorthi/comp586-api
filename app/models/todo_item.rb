# == Schema Information
#
# Table name: todo_items
#
#  id           :bigint(8)        not null, primary key
#  description  :string
#  status       :integer
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#  todo_list_id :bigint(8)
#
# Indexes
#
#  index_todo_items_on_todo_list_id  (todo_list_id)
#
# Foreign Keys
#
#  fk_rails_...  (todo_list_id => todo_lists.id)
#

class TodoItem < ApplicationRecord
  enum status: %i[pending done]

  validates :description, :status, presence: true

  belongs_to :todo_list
end
