class UserMailer < ApplicationMailer
  default from: 'notifications@homeboard-api.herokuapp.com'

  def activation
    @user = params[:user]
    @token = params[:raw_token]
    mail(to: @user.email, subject: 'Activation')
  end
end
