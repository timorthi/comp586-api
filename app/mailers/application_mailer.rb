class ApplicationMailer < ActionMailer::Base
  default from: 'notifications@homeboard-api.herokuapp.com'
  layout 'mailer'
end
