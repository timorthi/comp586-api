class TodoListsController < ApplicationController
  before_action :set_user, only: %i[index create]
  before_action :set_todo_list, only: :show
  before_action -> { verify_is_authed_user(params[:user_id] || @todo_list.user_id) }

  # GET /users/:user_id/todo_lists
  def index
    @todo_lists = @user.todo_lists
    render :index
  end

  # POST /users/:user_id/todo_lists
  def create
    @todo_list = TodoList.new(create_params.merge(user_id: @user.id))
    if @todo_list.save
      render :show, status: :created
    else
      render json: @todo_list.errors, status: :unprocessable_entity
    end
  end

  # GET /todo_lists/:id
  def show
    if @todo_list.present?
      render :show
    else
      head :not_found
    end
  end

  private

  def set_user
    @user = current_user
  end

  def set_todo_list
    @todo_list = TodoList.find_by_id(params[:id])
    head :not_found unless @todo_list.present?
  end

  def create_params
    params.require(:todo_list).permit(:title, :description)
  end
end
