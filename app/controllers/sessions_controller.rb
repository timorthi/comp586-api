class SessionsController < ApplicationController
  skip_before_action :authenticate_request

  # Authenticates a user given an email and password, and returns an
  # access token (JWT) for subsequent request authentication.
  #
  # @param user - an object containing the email:str and password:str keys
  # @return obj - { jwt:str, user:obj }
  def create
    @user = User.find_by_email(user_params[:email])
    if @user&.authenticate(user_params[:password])
      @jwt = JsonWebToken.encode(user_id: @user.id)
      render :create, status: :created
    else
      head :unauthorized
    end
  end

  private

  def user_params
    params.require(:user).permit(:email, :password)
  end
end
