class UsersController < ApplicationController
  skip_before_action :authenticate_request, only: :create

  def show
    @user = User.find_by_id(params[:id])
    if @user.present?
      render :show
    else
      head :not_found
    end
  end

  # Create a new user. Upon model validation, sends an email to the
  # newly registered user containing account activation information.
  #
  # @param user - an object containing the params for a user (see create_params below)
  # @return obj - contains the new user and jwt if successful, otherwise show model errors
  def create
    token = Token.generate(User::ACTIVATION_TOKEN_LENGTH)
    @user = User.new(create_params.merge(activation_token_digest: token[:digest],
                                       activation_sent_at: Time.zone.now))
    if @user.save
      UserMailer.with(user: @user, raw_token: token[:raw])
                .activation
                .deliver_now
      @jwt = JsonWebToken.encode(user_id: @user.id)
      render :create, status: :created
    else
      render json: @user.errors, status: :unprocessable_entity
    end
  end

  # Activates the currently authenticated user.
  # Returns an error if the the ID does not match the currently authenticated user.
  #
  # @param user - an object containing the params for a user (see activation_token_from_params below)
  # @return obj - contains the activated user if successful, otherwise show errors
  def update
    return head :forbidden unless params[:id].to_i == current_user.id
    return head :bad_request unless update_params[:activation_token].present?

    @user = current_user
    if @user.activate!(update_params[:activation_token])
      render :show, status: :ok
    else
      render json: @user.errors, status: :unprocessable_entity
    end
  end

  private

  # Format: { user: { first_name: '', last_name: '', ... } }
  def create_params
    params.require(:user).permit(
      :first_name,
      :last_name,
      :preferred_name,
      :email,
      :password
    )
  end

  # Format: { user: { activation_token: '' } }
  def update_params
    params.require(:user).permit(:activation_token)
  end
end
