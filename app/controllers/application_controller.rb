class ApplicationController < ActionController::API
  before_action :authenticate_request, :ensure_json_request

  def current_user
    @current_user
  end

  def verify_is_authed_user(user_id)
    head :forbidden unless current_user.id == user_id&.to_i
  end

  private

  def ensure_json_request
    request.format = :json
  end

  def authenticate_request
    @current_user = AuthenticationService.new(request.headers['Authorization']).call
    head :unauthorized unless @current_user
  end
end
