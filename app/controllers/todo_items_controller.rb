class TodoItemsController < ApplicationController
  before_action :set_todo_list, only: :create
  before_action :set_todo_item, only: %i[update destroy]
  before_action -> { verify_is_authed_user(@todo_list&.user_id || @todo_item.todo_list.user_id) }

  # POST /todo_lists/:todo_list_id/todo_items
  def create
    @todo_item = TodoItem.new(create_params.merge(todo_list_id: @todo_list.id, status: :pending))
    if @todo_item.save
      render :create, status: :created
    else
      render json: @todo_item.errors, status: :unprocessable_entity
    end
  end

  # PUT /todo_items/:id
  def update
    return head :bad_request unless TodoItem.statuses.include? update_params[:status]

    if @todo_item.update(update_params)
      render :update, status: :ok
    else
      render json: @todo_item.errors, status: :unprocessable_entity
    end
  end

  # DELETE /todo_items/:id
  def destroy
    head :ok if @todo_item.destroy
  end

  private

  def create_params
    params.require(:todo_item).permit(:description)
  end

  def update_params
    params.require(:todo_item).permit(:status)
  end

  def set_todo_list
    @todo_list = TodoList.find_by_id(params[:todo_list_id])
    head :not_found unless @todo_list.present?
  end

  def set_todo_item
    @todo_item = TodoItem.find_by_id(params[:id])
    head :not_found unless @todo_item.present?
  end
end
