require 'digest'

# Utility class for creating tokens to be used for user activation etc.
class Token
  HASH_FUNC = Digest::SHA256.new
  class << self
    def generate(length = 6)
      raise ArgumentError, 'Token length must be greater than 1' unless length > 1

      range = [*'0'..'9', *'a'..'z']
      token = Array.new(length) { range.sample }.join.upcase
      { raw: token, digest: HASH_FUNC.hexdigest(token) }
    end

    def verify(raw, digest)
      HASH_FUNC.hexdigest(raw.upcase) == digest
    end
  end
end
